<?php

use Illuminate\Support\Facades\Route;

$excepts = ['create', 'edit'];

Route::group(['prefix' => 'v1'], function () use ($excepts) {

    //PUBLIC ROUTES
    Route::group(['prefix' => 'auth'], function () {
        Route::get('login/{driver}', 'Api\v1\AuthController@redirectToProvider');
        Route::get('login/{driver}/callback', 'Api\v1\AuthController@handleProviderCallback');
        Route::post('/login', 'Api\v1\AuthController@login')->name('login');
        Route::get('/signup/activate/{token}', 'Api\v1\AuthController@signupActivate')->name('signup.active');
        Route::post('/password/forgot', 'Api\v1\AuthController@forgotPassword')->name('password.forgot');
        Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
        Route::post('/logout', 'Api\v1\AuthController@logout')->name('logout')->middleware('auth:api');
        Route::get('/user-info', 'Api\v1\AuthController@getUserLocal');
        Route::get('/user-info/{driver}', 'Api\v1\AuthController@getUserAuth0');
        Route::get('/user', 'Api\v1\AuthController@getUserAuth0');
        Route::post('/users', 'Api\v1\UserController@store');
    });

    Route::namespace('Api\v1')->middleware('auth:api')->group(function () use ($excepts) {
        Route::resource('/users', 'UserController');
        Route::resource('/partners', 'PartnerController');
        Route::post('/partners-product/{id}', 'PartnerController@saveProducts');
        Route::put('/partners-product/{id}', 'PartnerController@updateProducts');
        Route::resource('/partners-products', 'PartnerProductController', ['only' => ['index', 'show']]);
        Route::post('/partners-product-favorite/{id}', 'PartnerProductController@addFavorite');
        Route::resource('/banks', 'BankController');
        Route::resource('/indications', 'IndicationController');
        Route::resource('/management-points', 'ManagementPointController');
    });
});
