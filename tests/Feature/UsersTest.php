<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class UsersTest extends TestCase
{
    protected $endpoint = '/api/v1/users-suppliers';
    protected $model;

    protected function setUp()
    {
        parent::setUp();
        $this->model = $this->createModel();
    }

    /**
     * Test Index
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json('GET', $this->endpoint);
        $response->assertStatus(200)->json();
    }

    /**
     * Test Show
     *
     * @return void
     */
    public function testShow()
    {
        $response = $this->json('GET', $this->endpoint . '/' . $this->model['data']['uuid']);
        $response->assertStatus(200)->json();
    }

    /**
     * Test Show
     *
     * @return void
     */
    public function testDestroy()
    {
        $response = $this->delete($this->endpoint . '/' . $this->model['data']['uuid']);
        $response->assertStatus(200);
    }

    /**
     * Test Show
     *
     * @return void
     */
    public function testUpdate()
    {
        $user       = factory(User::class)->make();
        $userData   = [
            "name"                  => $user->name,
            "email"                 => $user->email,
            "password"              => $user->password,
            "password_confirmation" => $user->password,
        ];
        $dataUpdate = factory(User::class)->make(['user' => $userData])->toArray();
        $response   = $this->put($this->endpoint . '/' . $this->model['data']['uuid'], $dataUpdate);
        $response->assertStatus(200);
    }

    public function createModel()
    {
        $user     = factory(User::class)->make();
        $response = $this->post($this->endpoint, $user);
        $response->assertStatus(201);

        return json_decode($response->getContent(), true);
    }
}
