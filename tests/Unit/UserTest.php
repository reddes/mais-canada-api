<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class UsersTest extends TestCase
{
    protected $model;

    protected function setUp()
    {
        parent::setUp();
        $this->model = $this->createModel();
    }

    public function testCreate()
    {

    }

    public function createModel()
    {
        return factory(User::class)->create();
    }
}

