#!/usr/bin/env bash

php artisan config:clear
php artisan route:clear
php artisan cache:clear
php artisan view:clear
composer dumpautoload
#php artisan ide-helper:generate &> _ide_helper.log
#php artisan ide-helper:meta
