<?php

return [

    //AUTH
    'email_not_found' => 'Email not found.',
    'email_not_verified' => 'Email not verified. Please check your email to activate.',
    'email_not_active' => 'Your account has not been vetted by our staff yet. 
    We will contact as soon as it is accepted.',
    'login_success' => 'You have been successfully logged.',
    'email_already_registered' => 'E-mail already registered.',
    'logout_success' => 'You have been successfully logged out.',
    'login_error' => 'An error occurred on the login attempt.',
    'logout_error' => 'An error occurred on the logout attempt.',
    'invalid_credentials' => 'Username or Password incorrect.',
    'unauthenticated' => 'Unauthenticated.',

    'passwords.reset' => 'Password changed with success.',
    'passwords.token' => 'Token Invalid.',

    'passwords_reset_link_sent' => 'Reset link sent to your email.',
    'passwords_reset_link_sent_error' => 'Unable to send reset link',

    'post_code_find_success' => 'Address successfully found.',
    'post_code_find_error' => 'Address not found.',

    'sign_up_active_200' => 'Your account is now verified.',
    'sign_up_active_404' => 'This activation token is invalid.',
    'sign_up_active_422' => 'This activation token is invalid.',
    'deleted' => 'deleted',
    'updated' => 'updated',
    'list' => 'list',
    'created' => 'created',

];
