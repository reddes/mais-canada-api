<?php

return [

    //AUTH
    'email_not_found' => 'Email não encontrado.',
    'email_not_verified' => 'Email não verificado. Por favor, check a sua caixa de email.',
    'email_not_active' => 'Email não ativo. Por favor, entre em contato conosco para maiores informações.',
    'email_canceled' => 'Email cancelado. Por favor, entre em contato conosco para maiores informações.',
    'email_pending' => 'Sua conta encontra-se processo de ativação por nossa equipe. Por favor, aguarde que iremos 
    avisar assim que sua conta for ativada.',
    'login_success' => 'Autenticado com sucesso.',
    'email_already_registered' => 'E-mail já cadastrado.',
    'logout_success' => 'Você saiu do sistema.',
    'login_error' => 'An error occurred on the login attempt.',
    'logout_error' => 'An error occurred on the logout attempt.',
    'invalid_credentials' => 'Usuário ou senha incorreta.',
    'unauthenticated' => 'Não autenticado.',

    'passwords.reset' => 'Senha altera com sucesso.',
    'passwords.token' => 'Token Inválido.',

    'passwords_reset_link_sent' => 'Link para resetar a sua seha foi enviada para o seu email.',
    'passwords_reset_link_sent_error' => 'Não foi possível enviar o link para resetar a senha. Tente novamente.',


    'sign_up_active_200' => 'Sua conta foi ativada com sucesso.',
    'sign_up_active_404' => 'Seu token é inválido',
    'sign_up_active_422' => 'Seu token é inválido',
    'deleted' => 'deletad',
    'updated' => 'atualizdo',
    'list' => 'lista',
    'created' => 'criado',

];
