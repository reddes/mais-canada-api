<html>

<head>
    <title></title>
</head>
<style>
    body {
        padding: 20px 0 0 0;
        margin: 0;
        font-family: 'Roboto', sans-serif;
    }

    #content {
        background-color: white;
        min-height: 95%;
        padding: 10px;
    }
</style>
<body>

<div id="content">
    @if(isset($body))
        {!! $body !!}
    @endif

    <br>
    <p>
        <img src="{{env('APP_URL').'/logo_secondary.png'}}" title="" height="50">
    </p>
    <p>See you!</p>
    <p>Equipe Mais Canadá</p>
    <p>atendimento@maiscanada.com.br</p>
</div>
</body>
</html>
