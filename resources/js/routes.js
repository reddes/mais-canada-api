import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './vuex/store';

import Login from "./views/auth/Login";
import ForgotPassword from "./views/auth/ForgotPassword";
import ResetPassword from "./views/auth/ResetPassword";
import SuccessResetPassword from "./views/auth/SuccessResetPassword";
import SignUpActive from "./views/auth/SignUpActive";
import Dashboard from "./views/dashboard/Dashboard";


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: '',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/signup/activate/:token',
            name: 'signup-activate',
            component: SignUpActive
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: ForgotPassword
        },
        {
            path: '/reset-password/:token/:email',
            name: 'reset-password',
            component: ResetPassword
        },
        {
            path: '/success-reset-password',
            name: 'success-reset-password',
            component: SuccessResetPassword
        },
    ]
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    if (requiresAuth && !store.getters.isLoggedIn) {
        next('/login');
    } else if (requiresAuth && store.getters.isLoggedIn) {
        next();
    } else {
        next();
    }
});

export default router;