import router from '../routes';

export function loginHandler(response) {

    let token = response.data.data.token_type + ' ' + response.data.data.access_token;

    if (response.status == 200) {
        window.axios.defaults.headers.common['Authorization'] = token;

        axios
            .get("api/v1/auth/user")
            .then(response => {
                localStorage.setItem('user', JSON.stringify(response.data));
                localStorage.setItem('access_token', token);
                router.replace('dashboard');
            });
    } else {
        alert('Try again');
    }

    return response.data;
}

export function loginErrorHandler(error) {

    let response = error.response;

    if (response.status === 422) {
        alert('Please, check the user Email and password');
    } else if (response.status == 404) {
        alert('User/Email not Found');
    }

    return response.data;
}

export function logoutHandler() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
    delete axios.defaults.headers.common['Authorization'];
    return router.replace('login');
}

export function checkTokenExists() {
    return localStorage.getItem('access_token');
}

export function user() {
    return localStorage.getItem('user');
}