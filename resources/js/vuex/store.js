import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from "../routes";

Vue.use(Vuex);
export default new Vuex.Store({
    modules: {},
    state: {
        navigation: {
            drawer: true,
            mini: false,
            right: null
        },
        status: '',
        emailValidated: true,
        token: localStorage.getItem('access_token'),
        user: {},
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        get_user_request(state) {
            state.status = 'loading'
        },
        get_user_success(state, response) {
            state.status = 'success';
            state.user = response.data;
        },
        auth_success(state, response) {
            let token = response.data.data.token_type + ' ' + response.data.data.access_token;
            localStorage.setItem('access_token', token);
            window.axios.defaults.headers.common['Authorization'] = token;
            state.status = 'success';
            state.token = token;
        },
        auth_error(state) {
            state.status = 'error'
        },
        auth_logout(state) {
            state.status = '';
            state.token = ''
        },
        desktop(state) {
            // Clean app for login - TODO -> Blank all state later -> make a function
            state.navigation.drawer = true;
            state.navigation.mini = false;
            state.navigation.right = null;
        },
        mobile(state) {
            state.navigation.drawer = null;
            state.navigation.mini = false;
            state.navigation.right = null;
        },
        toggle(state) {
            state.navigation.drawer = state.navigation.drawer !== true ? true : null;
            state.navigation.mini = false;
            state.navigation.right = null;
        }
    },
    actions: {
        login({commit, dispatch}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post('api/v1/auth/login', user)
                    .then(response => {
                        commit('auth_success', response);
                        dispatch('get_user', response);
                        router.replace('dashboard');
                        resolve(response);
                    })
                    .catch((error) => {
                        let response = error.response;
                        reject(response);
                    });
            })
        },
        get_user({commit, dispatch}) {
            return new Promise((resolve, reject) => {
                commit('get_user_request');
                axios.get('api/v1/auth/user')
                    .then(response => {
                        commit('get_user_success', response);
                    })
                    .catch(function (error) {
                        reject(error.response);
                    });
            })
        },
        logout({commit}) {
            return new Promise((resolve, reject) => {
                commit('auth_logout');
                localStorage.removeItem('access_token');
                delete axios.defaults.headers.common['Authorization']
                router.replace('login');
                resolve()
            })
        },
        check_mobile({commit}, data) {
            return new Promise((resolve, reject) => {
                if (data === 'sm') {
                    commit('mobile');
                } else {
                    commit('desktop');
                }
                resolve()
            })
        },
        toggle({commit}) {
            return new Promise((resolve, reject) => {
                commit('toggle');
                resolve()
            })
        }
    },
    getters: {
        navigation: state => state.navigation,
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        token: state => state.token,
        user: state => state.user,
    }
})