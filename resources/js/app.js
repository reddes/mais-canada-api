/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueSweetalert2 from 'vue-sweetalert2';
import BootstrapVue from 'bootstrap-vue';
import VueMq from 'vue-mq';
import Notify from 'vue2-notify'

import 'bootstrap-css-only/css/bootstrap.min.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import 'mdbvue/build/css/mdb.css';

import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'vue-loading-overlay/dist/vue-loading.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';

import store from './vuex/store.js';
import Routes from './routes.js';
import App from '../views/App';


Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.use(Notify, {
    position: 'bottom-right',
    visibility: 5000
});
Vue.use(Vuetify, {
    iconfont: 'md'
});
Vue.use(VueMq, {
    breakpoints: { // default breakpoints - customize this
        sm: 450,
        md: 1250,
        lg: Infinity,
    },
    defaultBreakpoint: 'sm' // customize this for SSR
})
const app = new Vue({
    el: '#app',
    router: Routes,
    store,
    render: h => h(App),
});

export default app;
