<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\User::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'name' => $faker->name,
        'email' => $faker->email,
        'rg' => $faker->randomNumber(6),
        'cpf' => $faker->randomNumber(6) . '12345',
        'birth_date' => $faker->date(),
        'phone_number' => $faker->phoneNumber,
        'email_verified_at' => null,
        'password' => '12345678@',
        'remember_token' => null,
        'status' => $faker->randomElement(['active', 'pending']),
        'is_admin' => 0,
    ];
});
