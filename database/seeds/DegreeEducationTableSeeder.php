<?php

use Illuminate\Database\Seeder;

class DegreeEducationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $degrees = [
            ['code' => '', 'description' => 'Ensino médio'],
            ['code' => '', 'description' => 'Ensino fundamental'],
            ['code' => '', 'description' => 'Ensino superior'],
        ];

        foreach ($degrees as $degree) {
            \App\Models\DegreeEducation::create($degree);
        }

    }
}
