<?php

use Illuminate\Database\Seeder;

class OauthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'user_id' => null,
            'name' => 'Laravel Personal Access Client',
            'secret' => 'up5ubv1iEECOsHSDGN6Cu11z0wP3Q6NavbCzuq1X',
            'redirect' => '/',
            'personal_access_client' => false,
            'password_client' => true,
            'revoked' => false,
            'created_at' => \Illuminate\Support\Carbon::now(),
            'updated_at' => \Illuminate\Support\Carbon::now()
        ]);

        DB::table('oauth_clients')->insert([
            'user_id' => null,
            'name' => 'Laravel Personal Access Client',
            'secret' => 'h47oy51vk89kiwOm84J4iRbV670z5y8JSJUj4Mbn',
            'redirect' => '/',
            'personal_access_client' => false,
            'password_client' => true,
            'revoked' => false,
            'created_at' => \Illuminate\Support\Carbon::now(),
            'updated_at' => \Illuminate\Support\Carbon::now()
        ]);
    }
}
