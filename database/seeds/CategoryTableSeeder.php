<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = [
            ['id' => 1, 'description' => 'Bronze'],
            ['id' => 2, 'description' => 'Prata'],
            ['id' => 3, 'description' => 'Ouro'],
        ];

        foreach ($categorias as $categoria) {
            \App\Models\Category::create($categoria);
        }
    }
}
