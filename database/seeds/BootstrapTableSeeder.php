<?php

use Illuminate\Database\Seeder;

class BootstrapTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserDefaultSeeder::class); // User Admin
        $this->call(BankTableSeeder::class); // User Admin
        $this->call(DegreeEducationTableSeeder::class); // User Admin
        $this->call(CategoryTableSeeder::class); // User Admin
    }
}
