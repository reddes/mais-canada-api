<?php

use Illuminate\Database\Seeder;

class UserDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'User',
            'email' => 'user@maiscanada.com.br',
            'rg' => "00000000",
            'cpf' => "00000000000",
            'birth_date' => '1990-01-02',
            'email_verified_at' => now(),
            'password' => '123456',
            'remember_token' => null,
            'is_admin' => 1,
            'first_access' => 'N',
            'status' => 'active',
        ]);
        \App\Models\User::create([
            'name' => 'Rogerio',
            'email' => 'rogerio@faromidia.com.br',
            'rg' => "00000000",
            'cpf' => "00000000001",
            'birth_date' => '1990-01-02',
            'email_verified_at' => now(),
            'password' => '123456',
            'remember_token' => null,
            'is_admin' => 1,
            'first_access' => 'N',
            'status' => 'active',
        ]);
        \App\Models\User::create([
            'name' => 'Wellington',
            'email' => 'wellington.alves.gomes@gmail.com',
            'rg' => "00000000",
            'cpf' => "00000000002",
            'birth_date' => '1990-01-02',
            'email_verified_at' => now(),
            'password' => '123456',
            'remember_token' => null,
            'is_admin' => 1,
            'first_access' => 'N',
            'status' => 'active',
        ]);
    }
}
