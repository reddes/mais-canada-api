<?php

use Illuminate\Database\Seeder;

use App\Models\Uf;

class UfTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estadosBrasileiros = [
            ['code' => 'AC', 'description' => 'Acre'],
            ['code' => 'AL', 'description' => 'Alagoas'],
            ['code' => 'AP', 'description' => 'Amapá'],
            ['code' => 'AM', 'description' => 'Amazonas'],
            ['code' => 'BA', 'description' => 'Bahia'],
            ['code' => 'CE', 'description' => 'Ceará'],
            ['code' => 'DF', 'description' => 'Distrito Federal'],
            ['code' => 'ES', 'description' => 'Espírito Santo'],
            ['code' => 'GO', 'description' => 'Goiás'],
            ['code' => 'MA', 'description' => 'Maranhão'],
            ['code' => 'MT', 'description' => 'Mato Grosso'],
            ['code' => 'MS', 'description' => 'Mato Grosso do Sul'],
            ['code' => 'MG', 'description' => 'Minas Gerais'],
            ['code' => 'PA', 'description' => 'Pará'],
            ['code' => 'PB', 'description' => 'Paraíba'],
            ['code' => 'PR', 'description' => 'Paraná'],
            ['code' => 'PE', 'description' => 'Pernambuco'],
            ['code' => 'PI', 'description' => 'Piauí'],
            ['code' => 'RJ', 'description' => 'Rio de Janeiro'],
            ['code' => 'RN', 'description' => 'Rio Grande do Norte'],
            ['code' => 'RS', 'description' => 'Rio Grande do Sul'],
            ['code' => 'RO', 'description' => 'Rondônia'],
            ['code' => 'RR', 'description' => 'Roraima'],
            ['code' => 'SC', 'description' => 'Santa Catarina'],
            ['code' => 'SP', 'description' => 'São Paulo'],
            ['code' => 'SE', 'description' => 'Sergipe'],
            ['code' => 'TO', 'description' => 'Tocantins']
        ];

        foreach ($estadosBrasileiros as $estado) {
            Uf::create($estado);
        }
    }
}
