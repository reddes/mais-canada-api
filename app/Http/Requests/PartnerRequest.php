<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'name_responsible' => 'required',
            'cpf_cnpj' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'logotipo' => '',
        ];
    }

    public function messages()
    {
        return [
            'name' => 'Nome',
            'name_responsible' => 'Nome do resposanvel',
            'cpf_cnpj' => 'CPF/CNPJ',
            'phone_number' => 'Telefone',
            'email' => 'Email',
            'logotipo' => 'Logotipo',
        ];
    }
}
