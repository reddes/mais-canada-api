<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Http\Requests\UsersClientRequest;
use App\Services\UsersClient\UserService;
use Illuminate\Http\Request;

class UserController extends AbstractController
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsersClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UsersClientRequest $request)
    {
        $data = $this->service->create($request);
        return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersClientRequest $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        $data = $this->service->find($id);
        return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
