<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Http\Requests\UsersClientRequest;
use App\Services\Bank\BankService;
use Illuminate\Http\Request;

class BankController extends AbstractController
{
    /**
     * @var BankService
     */
    protected $service;

    /**
     * BankController constructor.
     *
     * @param BankService $service
     */
    public function __construct(BankService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsersClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $data = $this->service->create($request);
        return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersClientRequest $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        $data = $this->service->find($id);
        return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
