<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\AbstractController;
use App\Services\PartnerProduct\PartnerProductService;
use App\Facades\Response;

class PartnerProductController extends AbstractController
{
    /**
     * @var PartnerProductService
     */
    protected $service;

    /**
     * IndicationController constructor.
     *
     * @param PartnerProductService $service
     */
    public function __construct(PartnerProductService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $id
     * @return
     */
    public function addFavorite($id)
    {
        $data = $this->service->addFavorite($id);
        return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
