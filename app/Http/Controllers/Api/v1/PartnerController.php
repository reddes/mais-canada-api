<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Http\Requests\PartnerRequest;
use App\Models\PartnerProduct;
use App\Services\Partner\PartnerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helpers;

class PartnerController extends AbstractController
{
    /**
     * @var PartnerService
     */
    protected $service;

    /**
     * IndicationController constructor.
     *
     * @param PartnerService $service
     */
    public function __construct(PartnerService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PartnerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PartnerRequest $request)
    {
        try {
            DB::beginTransaction();
            $request['user_id'] = $request->user()->id;
            $request['logotipo'] = Helpers::saveFile($request, 'logotipo', 'file');
            $request['partner_address'] = json_decode($request->get('partner_address'), true);
            $data = $this->service->create($request);

            DB::commit();
            return Response::getJsonResponse('created', '', \Illuminate\Http\Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            DB::rollBack();
            Helpers::deleteFile($request['logotipo']);
            Log::info($exception->getMessage());

            return Response::getJsonResponse('error', '', \Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(PartnerRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $request['partner_address'] = json_decode($request->get('partner_address'), true);

            if ($request->file('file')) {
                $find = $this->service->find($id);
                if ($find) {
                    Helpers::deleteFile($find->logotipo);
                }
                $request['logotipo'] = Helpers::saveFile($request, 'logotipo', 'file');
            }

            $this->service->update($request, $id);
            $data = $this->service->find($id);
            DB::commit();

            return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
        } catch (\Exception $exception) {
            DB::rollBack();
            Helpers::deleteFile($request['logotipo']);
            Log::info($exception->getMessage());

            return Response::getJsonResponse('error', '', \Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function saveProducts(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $request['partner_product'] = json_decode($request->get('partner_product'), true);
            $request['image'] = Helpers::saveFile($request, 'products', 'file');
            $data = $this->service->saveProducts($request, $id);
            DB::commit();

            return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_OK);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info($exception->getMessage());
            Helpers::deleteFile($request['image']);
            return Response::getJsonResponse('error', '', \Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateProducts(Request $request, $id)
    {
        try {
            $request['partner_product'] = json_decode($request->get('partner_product'), true);
            if ($request->file('file')) {
                $find = PartnerProduct::find($id);
                if ($find) {
                    Helpers::deleteFile($find->image);
                }
                $request['image'] = Helpers::saveFile($request, 'products', 'file');
            }
            $data = $this->service->updateProducts($request, $id);

            return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_OK);
        } catch (\Exception $exception) {
            DB::rollBack();
            Helpers::deleteFile($request['image']);
            Log::info($exception->getMessage());

            return Response::getJsonResponse('error', '', \Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $uuid
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($uuid)
    {
        $data = $this->service->destroy($uuid);
        return Response::getJsonResponse('deleted', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
