<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Indication;
use App\Models\User;
use App\Notifications\NewIndicationAdminNotification;
use App\Notifications\NewIndicationIndicadoNotification;
use App\Notifications\NewIndicationIndicadorNotification;
use App\Services\Indication\IndicationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class IndicationController extends AbstractController
{
    /**
     * @var IndicationService
     */
    protected $service;

    /**
     * IndicationController constructor.
     *
     * @param IndicationService $service
     */
    public function __construct(IndicationService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $request['user_id'] = $user->id;
        $data = $this->service->create($request);

        $admins = new User();
//        $admins->email = 'souindicador@maiscanada.com.br';
        $admins->email = 'wellington.alves.gomes@gmail.com';
        $indicado = new User();
        $indicado->email = $data->email;

        if ($data) {
            Notification::send($user, new NewIndicationIndicadorNotification($data));
            Notification::send($indicado, new NewIndicationIndicadoNotification($user));
            Notification::send($admins, new NewIndicationAdminNotification($data));
        }
        return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        $data = $this->service->find($id);
        return Response::getJsonResponse('updated', '', \Illuminate\Http\Response::HTTP_OK);
    }
}
