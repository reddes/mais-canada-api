<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Services\ManagementPoint\ManagementPointService;
use Illuminate\Http\Request;

class ManagementPointController extends AbstractController
{
    /**
     * @var ManagementPointService
     */
    protected $service;

    /**
     * IndicationController constructor.
     *
     * @param ManagementPointService $service
     */
    public function __construct(ManagementPointService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $data = $this->service->create($request);
        return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        $data = $this->service->find($id);
        return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
