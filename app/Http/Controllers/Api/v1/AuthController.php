<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Notifications\RegisterNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    /**
     * Login
     * @param LoginRequest $request
     *
     * @return Response|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            $user = User::where('email', $request->get('email'))->first();

            if (!$user) {
                return Response::getJsonResponse(
                    'email_not_found',
                    $request->get('email'),
                    \Illuminate\Http\Response::HTTP_NOT_FOUND
                );
            }

            if ($user->status == 'pending') {
                return Response::getJsonResponse(
                    'email_pending',
                    $user->email,
                    \Illuminate\Http\Response::HTTP_UNAUTHORIZED
                );
            }

            if ($user->status == 'inactive') {
                return Response::getJsonResponse(
                    'email_not_active',
                    $user->email,
                    \Illuminate\Http\Response::HTTP_UNAUTHORIZED
                );
            }

            if ($user->status == 'reject') {
                return Response::getJsonResponse(
                    'email_canceled',
                    $user->email,
                    \Illuminate\Http\Response::HTTP_UNAUTHORIZED
                );
            }

            $request = Request::create('/oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => config('services.passport_client.id'),
                'client_secret' => config('services.passport_client.secret'),
                'username' => $request->get('email'),
                'password' => $request->get('password'),
            ]);
            $attempt = app()->handle($request);

            if ($attempt->getStatusCode() == \Illuminate\Http\Response::HTTP_UNAUTHORIZED) {
                return Response::getJsonResponse(
                    json_decode($attempt->getContent())->error,
                    'Error',
                    \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $content = json_decode($attempt->getContent());
            return Response::getJsonResponse(
                'login_success',
                $content,
                \Illuminate\Http\Response::HTTP_OK
            );
        } catch (\Exception $e) {
            \Debugbar::addThrowable($e);
            return Response::getJsonResponse(
                'login_error',
                $e->getMessage(),
                \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * SignupActivate
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function signupActivate($token)
    {
        try {
            $user = User::where('remember_token', $token)->first();

            if (!$user) {
                return Response::getJsonResponse(
                    'sign_up_active_404',
                    $user,
                    \Illuminate\Http\Response::HTTP_NOT_FOUND
                );
            }

            $user->email_verified_at = Carbon::now();
            $user->remember_token = '';

            $user->status = 'trial';

            $user->save();

            return Response::getJsonResponse(
                'sign_up_active_200',
                $user,
                \Illuminate\Http\Response::HTTP_OK
            );
        } catch (\Exception $e) {
            \Debugbar::addThrowable($e);
            return Response::getJsonResponse(
                'sign_up_active_422',
                $e->getMessage(),
                \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function forgotPassword(Request $request)
    {
        $email = $request->get('email');
        if (!$email) {
            throw new \Exception('Email não informado');
        }
        $user = User::where('email', $email)->first();

        if (!$user) {
            throw new \Exception('Email não encontrado');
        }
        $password = $this->gerarSenha(6, false, true, true, false);
        $user->sendPasswordReseted($password);
        $user->password = bcrypt($password);
        $user->save();

        return response('success', 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            $revoked = $request->user()->token()->revoke();

            return Response::getJsonResponse(
                'logout_success',
                $revoked,
                \Illuminate\Http\Response::HTTP_OK
            );
        } catch (\Exception $e) {
            \Debugbar::addThrowable($e);
            return Response::getJsonResponse(
                'logout_error',
                $e->getMessage(),
                \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Redirect the user to the Driver authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver)
    {
        return Socialite::driver($driver)->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($driver)
    {
        $user = Socialite::driver($driver)->stateless()->user();
        $userSystem = User::where('email', $user->email)->first();

        if (!$userSystem) {
            $data = [
                'name' => $user->name,
                'email' => $user->email,
            ];
            return redirect(env('APP_WEB') . '/register?user=' . json_encode($data));
        }

        return redirect(env('APP_WEB') . '/callback?token=' . $user->token . '&driver=' . $driver);
    }

    /**
     * Get User Authenticated
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function getUserLocal()
    {
        $user = auth()->guard('api')->user();
        $user->exp = Carbon::now()->addHours(24);
        $user->firstAccess = $user->first_access;
        $user->userId = $user->id;

        return $user;
    }

    /**
     * Get User Socialite
     * @param $driver
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function getUserAuth0($driver)
    {
        $token = request()->bearerToken();
        $user = Socialite::driver($driver)->userFromToken($token);
        $findUser = User::where('email', $user->email)->first();
        $this->sendRegisterNotification($findUser);

        if (!$this->checkUser($findUser)) {
            return Response::getJsonResponse(
                'email_not_active',
                $user->email,
                \Illuminate\Http\Response::HTTP_UNAUTHORIZED
            );
        }
        $user->exp = Carbon::now()->addHours(24);
        $user->firstAccess = $findUser->first_access;
        $user->userId = $findUser->id;
        $user->token = $findUser->createToken($findUser->email)->accessToken;

        return response()->json($user, 200);
    }

    /**
     * Check User Status
     * @param $user
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function checkUser($user)
    {
        if (is_null($user->status) || $user->status == 'pending') {
            return false;
        }
        return true;
    }

    /**
     * Send notification if user recently registered
     * @param $user
     */
    public function sendRegisterNotification($user)
    {
        if ($user->wasRecentlyCreated) {
            $user->notify(new RegisterNotification());
        }
    }

    /**
     * @param $tamanho
     * @param $maiusculas
     * @param $minusculas
     * @param $numeros
     * @param $simbolos
     * @return bool|string
     */
    public function gerarSenha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos)
    {
        $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ";
        $mi = "abcdefghijklmnopqrstuvyxwz";
        $nu = "0123456789";
        $si = "!@#$%¨&*()_+=";

        $senha = '';

        if ($maiusculas) {
            $senha .= str_shuffle($ma);
        }

        if ($minusculas) {
            $senha .= str_shuffle($mi);
        }

        if ($numeros) {
            $senha .= str_shuffle($nu);
        }

        if ($simbolos) {
            $senha .= str_shuffle($si);
        }

        return substr(str_shuffle($senha), 0, $tamanho);
    }
}
