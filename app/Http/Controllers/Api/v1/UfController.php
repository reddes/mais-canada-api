<?php

namespace App\Http\Controllers\Api\v1;

use App\Facades\Response;
use App\Http\Controllers\AbstractController;
use App\Http\Requests\UsersClientRequest;
use App\Services\Bank\BankService;
use App\Services\Uf\UfService;
use Illuminate\Http\Request;

class UfController extends AbstractController
{
    /**
     * @var BankService
     */
    protected $service;

    /**
     * UfController constructor.
     *
     * @param UfService $service
     */
    public function __construct(UfService $service)
    {
        $this->service = $service;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UsersClientRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $data = $this->service->create($request);
        return Response::getJsonResponse('created', $data, \Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsersClientRequest $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        $data = $this->service->find($id);
        return Response::getJsonResponse('updated', $data, \Illuminate\Http\Response::HTTP_OK);
    }
}
