<?php


namespace App\Http\Controllers;

use App\Facades\Response;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\FileNotFoundException;

abstract class AbstractController extends Controller
{
    /**
     * @var $service
     */
    protected $service;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->service->all();
    }

    /**
     * Display the specified resource.
     *
     * @param $uuid
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($uuid)
    {
        $data = $this->service->find($uuid);
        return Response::getJsonResponse('list', $data, \Illuminate\Http\Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $uuid
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($uuid)
    {
        $this->service->destroy($uuid);
        return Response::getJsonResponse('deleted', '', \Illuminate\Http\Response::HTTP_OK);
    }
}
