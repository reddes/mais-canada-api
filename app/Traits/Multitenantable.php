<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;

trait Multitenantable
{

    /**
     *
     */
    protected static function bootMultitenantable()
    {
        if (auth()->guard('api')->check()) {
            static::addGlobalScope('user_id', function (Builder $builder) {
                $builder->where('id', auth()->guard('api')->id());
            });
        }
    }
}