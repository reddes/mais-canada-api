<?php

namespace App\Models;

class Complement extends AbstractModel
{
    protected $fillable = ['user_id', 'interest_to_study_canada', 'departure_date'];
}
