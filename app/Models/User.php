<?php

namespace App\Models;

use App\Notifications\SendPasswordResetedNotification;
use App\Notifications\SignupActivateNotification;
use Emadadly\LaravelUuid\Uuids;
use App\Notifications\PasswordResetRequestNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Reddes\MappableModels\Traits\HasNestedAttributes;

class User extends Authenticatable
{
    use HasApiTokens, HasNestedAttributes, Notifiable, Uuids, SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'password',
        'rg',
        'cpf',
        'birth_date',
        'phone_number',
        'optional_number',
        'first_access',
        'photo',
        'status',
        'category_id',
        'job',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activation_token'
    ];

    /**
     * The nested's (relations)
     *
     * @var array
     */
    protected $nested = [
        'address',
        'bank',
        'social',
        'user_files',
        'education',
        'complement',
    ];

    /**
     * The with's (relations)
     *
     * @var array
     */
    public $with = [
        'address',
        'bank',
        'social',
        'userFiles',
        'education',
        'complement',
    ];

    /**
     * Send the sign up active notification.
     *
     * @return void
     */
    public function sendSignUpActiveNotification()
    {
        $this->notify(new SignupActivateNotification());
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetRequestNotification($token));
    }

    /**
     * Send the password reseted.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordReseted($password)
    {
        $this->notify(new SendPasswordResetedNotification($password));
    }

    /**
     *  Mutator Password
     *
     * @param $pass
     */
    public function setPasswordAttribute($pass)
    {
        if (Hash::needsRehash($pass)) {
            $this->attributes['password'] = Hash::make($pass);
        } else {
            $this->attributes['password'] = $pass;
        }
    }

    /**
     * @param $value
     * @return false|string
     */
    public function setBirthDateAttribute($value)
    {
        return $this->attributes['birth_date'] = date('Y-m-d', strtotime($value));
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getBirthDateAttribute($value)
    {
        if ($value) {
            return date('m-d-Y', strtotime($value));
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class)->withDefault(function ($address) {
            return $address->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bank()
    {
        return $this->hasOne(UserBank::class)->withDefault(function ($bank) {
            return $bank->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function education()
    {
        return $this->hasOne(Education::class)->withDefault(function ($education) {
            return $education->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userFiles()
    {
        return $this->hasOne(UserFile::class)->withDefault(function ($files) {
            return $files->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function social()
    {
        return $this->hasOne(Social::class)->withDefault(function ($social) {
            return $social->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function complement()
    {
        return $this->hasOne(Complement::class)->withDefault(function ($complement) {
            return $complement->valuesNull();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

//    public function getStatusAttribute($value)
//    {
//        switch ($value) {
//            case 'pending':
//                return 'Pré cadastro';
//                break;
//            case 'canceled':
//                return 'Recusado';
//                break;
//            case 'active':
//                return 'Finalizado';
//                break;
//        }
//    }
}
