<?php

namespace App\Models;


class UserBank extends AbstractModel
{
    protected $fillable = ['user_id', 'bank_id', 'type', 'agency', 'account'];


    protected $with = ['bank'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
