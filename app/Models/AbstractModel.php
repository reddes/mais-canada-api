<?php

namespace App\Models;

use App\Events\DeleteNested;
use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Reddes\MappableModels\MappableModel;
use Reddes\MappableModels\Traits\HasNestedAttributes;

abstract class AbstractModel extends Model
{
    use HasNestedAttributes, SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleting' => DeleteNested::class,
    ];

    //protected $dates = ['deleted_at'];
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The nested's (relations)
     *
     * @var array
     */
    public $nested = [];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Audit Relation
     * @return mixed
     */
    public function audit()
    {
        return $this->morphMany(AuditAction::class, 'object');
    }

    /**
     * Return empty values when relation is null
     * @return Collection
     */
    public function valuesNull()
    {
        $columns = [];
        foreach ($this->getFillable() as $field) {
            $columns[$field] = '';
        }

        return Collection::make($columns);
    }
}
