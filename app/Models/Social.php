<?php

namespace App\Models;

class Social extends AbstractModel
{
    protected $fillable = ['facebook', 'twitter', 'instagram', 'linkedin'];
}
