<?php

namespace App;

use App\Models\AbstractModel;

class Indication extends AbstractModel
{

    /**
     * @param $value
     */
    public function setDataViagemAttribute($value)
    {
        $this->attributes['data_viagem'] = date('Y-m-d', strtotime($value));
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getDataViagemAttribute($value)
    {
        return date('d-m-Y', strtotime($value));
    }

    /**
     * @param $value
     * @return string
     */
    public function getCidadeDestinoAttribute($value)
    {
        return 'Vancouver';
    }
}
