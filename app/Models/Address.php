<?php

namespace App\Models;


class Address extends AbstractModel
{
    protected $fillable = ['zip_code', 'street', 'number', 'complement', 'state', 'number', 'city'];
}
