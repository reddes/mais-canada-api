<?php

namespace App\Models;


class UserFile extends AbstractModel
{
    protected $fillable = ['rg', 'cpf_cnh', 'proof_salary'];
}
