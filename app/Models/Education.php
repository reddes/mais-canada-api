<?php

namespace App\Models;


class Education extends AbstractModel
{
    protected $fillable = ['user_id', 'degree_education_id'];

    protected $with = ['degreeEducation'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function degreeEducation()
    {
        return $this->belongsTo(DegreeEducation::class);
    }
}
