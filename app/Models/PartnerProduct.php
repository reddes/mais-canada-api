<?php

namespace App\Models;


use Illuminate\Support\Facades\Storage;

class PartnerProduct extends AbstractModel
{
    protected $guarded = ['file', 'image_url'];

    protected $appends = ['image_url'];

    /**
     * @param $value
     * @return false|string
     */
    public function setDateExpirationAttribute($value)
    {
        return $this->attributes['date_expiration'] = date('Y-m-d', strtotime($value));
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getImageUrlAttribute()
    {
        if (!$this->attributes['image']) {
            return url('NoPicAvailable.png');
        }
        return Storage::disk('public')->url($this->attributes['image']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites()
    {
        return $this->hasMany(PartnerProductsFavorite::class, 'partner_product_id')
            ->where('user_id', '=', request()->user()->id);
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getDateExpirationAttribute($value)
    {
        return date('d-m-Y', strtotime($value));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function managementPoint()
    {
        return $this->hasOne(ManagementPoint::class, 'partner_product_id')
            ->where('user_id', '=', request()->user()->id);
    }
}
