<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Partner extends AbstractModel
{
    public $nested = [
        'partner_address',
//        'partner_product',
    ];

    protected $guarded = ['file', 'partner_product', 'logotipo_url'];

    protected $appends = ['logotipo_url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function partnerAddress()
    {
        return $this->hasOne(PartnerAddress::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partnerProduct()
    {
        return $this->hasMany(PartnerProduct::class);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getLogotipoUrlAttribute()
    {
        return Storage::disk('public')->url($this->attributes['logotipo']);
    }
}
