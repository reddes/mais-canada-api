<?php


namespace App\Repositories;

use App\Models\Partner;

class PartnerRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
        'partnerAddress',
        'partnerProduct',
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'email',
        'cpf_cnpj',
        'status',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Partner::class;
    }
}
