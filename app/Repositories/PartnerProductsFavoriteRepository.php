<?php


namespace App\Repositories;

use App\Models\PartnerProductsFavorite;

class PartnerProductsFavoriteRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PartnerProductsFavorite::class;
    }
}
