<?php

namespace App\Repositories;

use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

abstract class AbstractRepository extends \Prettus\Repository\Eloquent\BaseRepository
{

    public $relationships = [];

    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws Exception
     */
    public function create(array $attributes)
    {
        $result = parent::create($attributes);

        return $this->with($this->relationships)->find($result->id);
    }

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws Exception
     */
    public function update(array $attributes, $id)
    {
        parent::update($attributes, $id);

        return $this->with($this->relationships)->find($id);
    }

    /**
     * Update or Create an entity in repository
     *
     * @throws ValidatorException
     * @throws Exception
     *
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
        $result = parent::updateOrCreate($attributes, $values);
        $primaryKey = $result->getKeyName();

        return $this->with($this->relationships)->find($result->$primaryKey);
    }

    /**
     * Retrieve first data of repository, or return new Entity
     *
     * @param array $attributes
     *
     * @return mixed
     * @throws Exception
     */
    public function firstOrNew(array $attributes = [])
    {
        return parent::firstOrNew($attributes);
    }

    /**
     * Retrieve first data of repository, or create new Entity
     *
     * @param array $attributes
     *
     * @return mixed
     * @throws Exception
     */
    public function firstOrCreate(array $attributes = [])
    {
        $result = parent::firstOrCreate($attributes);

        return $this->with($this->relationships)->find($result->id);
    }

    /**
     * Overriding Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function delete($id)
    {
        $request = $this->with($this->relationships)->find($id);
        parent::delete($id);

        return $request;
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Overriding Load relations
     *
     * @param array|string $relations
     *
     * @return AbstractRepository|\Prettus\Repository\Eloquent\BaseRepository
     */
    public function with($relations)
    {
        return !empty($relations) ? parent::with($relations) : $this;
    }
}
