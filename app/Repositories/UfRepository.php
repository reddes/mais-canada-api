<?php


namespace App\Repositories;

use App\Models\Bank;
use App\Models\Uf;
use App\Models\User;

class UfRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Uf::class;
    }
}
