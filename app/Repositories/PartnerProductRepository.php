<?php


namespace App\Repositories;

use App\Models\PartnerProduct;

class PartnerProductRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
        'partner',
        'favorites',
        'managementPoint',
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partner_id',
        'featured',
        'favorites.status',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PartnerProduct::class;
    }
}
