<?php


namespace App\Repositories;

use App\Models\User;

class UserRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
        'category'
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'email',
        'cpf' => 'like',
        'category_id' => 'like',
        'status' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}
