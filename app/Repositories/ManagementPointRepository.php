<?php


namespace App\Repositories;

use App\Models\ManagementPoint;

class ManagementPointRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
        'partnerProduct',
        'user',
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ManagementPoint::class;
    }
}
