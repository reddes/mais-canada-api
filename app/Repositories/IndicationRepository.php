<?php


namespace App\Repositories;

use App\Indication;

class IndicationRepository extends AbstractRepository
{
    /**
     * Relationships (with)
     * @var array
     */
    public $relationships = [
    ];

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'email' => 'like',
        'cidade_destino',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Indication::class;
    }
}
