<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @mixin \App\Helpers\Response
 */

class Response extends Facade
{
    /**
     * @return string
     */
    public static function getFacadeAccessor()
    {
        return 'response';
    }
}
