<?php

namespace App\Observers;

use App\Helpers\Helpers;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerObserver
{
    /**
     * Handle the Partner "created" event.
     *
     * @param Partner $partner
     * @return void
     */
    public function created(Partner $partner)
    {
    }

    /**
     * Handle the Partner "created" event.
     *
     * @param Partner $partner
     * @param Request $request
     * @return void
     */
    public function creating(Partner $partner)
    {
    }

    /**
     * Handle the Partner "updated" event.
     *
     * @param Partner $partner
     * @return void
     */
    public function updated(Partner $partner)
    {
    }

    /**
     * Handle the Partner "updating" event.
     *
     * @param Partner $partner
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updating(Partner $partner)
    {
    }

    /**
     * Handle the Partner "deleted" event.
     *
     * @param Partner $partner
     * @return void
     */
    public function deleted(Partner $partner)
    {
        //
    }

    /**
     * Handle the Partner "restored" event.
     *
     * @param Partner $partner
     * @return void
     */
    public function restored(Partner $partner)
    {
        //
    }

    /**
     * Handle the Partner "force deleted" event.
     *
     * @param Partner $partner
     * @return void
     */
    public function forceDeleted(Partner $partner)
    {
        //
    }
}
