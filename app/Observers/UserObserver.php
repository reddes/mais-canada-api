<?php

namespace App\Observers;

use App\Models\User;
use App\Notifications\UserActiveNotification;
use App\Notifications\UserInactiveNotification;
use App\Notifications\UserRejectNotification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
//        $userCreated = User::uuid($user->uuid);
//        $userCreated->remember_token = str_random(60);
//        $userCreated->save();
//        $userCreated->sendSignUpActiveNotification();
    }

    /**
     * Handle the user "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        if ($user->status == 'active') {
            if (!$user->password) {
                $password = str_random(6);
                $user->password = Hash::make($password);
                $user->save();
                $user->notify(new UserActiveNotification($password));
            }
        }

        if ($user->status == 'inactive') {
            Notification::send($user, new UserInactiveNotification());
        }

        if ($user->status == 'reject') {
            Notification::send($user, new UserRejectNotification());
        }
    }

    /**
     * Handle the user "updating" event.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updating(User $user)
    {
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
