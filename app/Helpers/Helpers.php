<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Helpers
{
    /**
     * @param $request Request
     * @param string $folder
     * @param string $key
     * @return string
     */
    public static function saveFile($request, $folder = '', $key = 'file')
    {
        if (!$request->hasFile($key)) {
            return null;
        }
        $path = Storage::disk('public')->put($folder, $request->file($key));

        return $path;
    }

    /**
     * @param $path
     * @return string
     */
    public static function deleteFile($path)
    {
        $path = Storage::disk('public')->delete($path);

        return $path;
    }
}
