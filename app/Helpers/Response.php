<?php

namespace App\Helpers;

class Response
{
    /**
     * @param $message
     * @param $data
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getJsonResponse($message, $data, $code)
    {
        return response()->json(['message' => __('messages.' . $message), 'data' => $data], $code);
    }
}
