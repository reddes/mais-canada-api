<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegisterNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Legal! Você quer ser Parceiro Indicador do nosso clube!';
        $body = '
        <h2>Sim, você pode se tornar um Parceiro Indicador!</h2>
        <p>Obrigado por querer continuar conosco, mas agora como Parceiro Indicador. Recebemos sua solicitação de cadastro e vamos analisá-la.</p>
        <p>Em breve, você pode se juntar ao time de membros de nosso clube que explica aos estudantes brasileiros as vantagens de comprar um intercâmbio no Canadá usando o nosso clube.</p>
        <p>E por cada estudante que você nos indicar e que for mesmo para o Canadá, você receberá pontos de bonificação por nos ajudar nessa conquista.</p>
        <p>Como você sabe, esses pontos podem ser trocados por cursos e serviços de nossos parceiros canadenses e brasileiros. Mas se você preferir, pode converter os pontos em cashback.</p>
        ';

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.mail-template', compact('body'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
