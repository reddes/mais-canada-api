<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class TrocaPorProdutoAprovadoNotification extends Notification
{
    use Queueable;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Seu cashback está chegando!';
        $body = '
        <h2>Seu cashback está chegando!</h2>
        <p>Parabéns! Seu cashback está a caminho. Sua solicitação foi aprovada e você receberá o dinheiro em conta na próxima data de pagamento.</p>
        <p>Caso você tenha alguma dúvida, envie um e-mail para: souindicador@maiscanada.com.br.</p>             
        ';

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.mail-template', compact('body'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
