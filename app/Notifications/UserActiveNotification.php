<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserActiveNotification extends Notification
{
    use Queueable;
    private $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        //
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Retorno sobre o pedido de Parceiro Indicador';
        $body = '
        <h2>Você acaba de se tornar um Parceiro Indicador!</h2>   
        <p>Parabéns! Seu cadastro de Parceiro Indicador foi aprovado. Agora, você faz parte do time de membros que ajuda mais estudantes brasileiros a conhecer as vantagens de comprar um intercâmbio no Canadá com a gente!</p>
        <p>Comece agora a indicar estudantes para o nosso clube. Por cada indicação feita, você receberá pontos. Por cada estudante que efetivamente for estudar no Canadá, você receberá uma bonificação em pontos para usar como quiser: pode trocar por cursos e serviços de parceiros canadenses e brasileiros ou solicitar o cashback.</p>            
        <p>Nosso clube é novo. Se você enfrentar dificuldades para indicar estudantes pra gente, entre em contato para podermos te ajudar: souindicador@maiscanada.com.br.</p>
        <p><b>Seus dados de acesso:</b></p>
        <p>Usuário: ' . $notifiable->email . '</p>
        <p>Senha: ' . $this->password . '</p>
        ';

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.mail-template', compact('body'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
