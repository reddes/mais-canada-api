<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewIndicationIndicadorNotification extends Notification
{
    use Queueable;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Obrigado por indicar!';
        $body = '
        <h2>Recebemos sua indicação</h2>   
        <p>Agradecemos sua confiança em ter indicado o(a) estudante ' . $this->data->name . ' para o nosso clube.</p>
        <p>Conte com a gente para fazer o sonho de cada estudante se tornar realidade no Canadá.</p>                   
        ';

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.mail-template', compact('body'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
