<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRejectNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'Retorno sobre o pedido de Parceiro Indicador';
        $body = '
        <h2>Retorno sobre o pedido de Parceiro Indicador</h2>   
        <p>De acordo com a nossa análise do seu perfil, baseada em critérios definidos internamente, 
        infelizmente não podemos torná-lo um Parceiro Indicador nesse momento.</p>
        <p>Entendemos que isso pode ser decepcionante. Gostaríamos de lembrar que, no futuro, 
        pode ser que nossa análise tenha evoluído o bastante para te liberar um convite e,
         se isso acontecer, entraremos 
        em contato.</p>            
        ';

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.mail-template', compact('body'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
