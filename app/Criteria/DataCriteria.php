<?php

namespace App\Criteria;

use Illuminate\Support\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DataCriteria.
 *
 * @package namespace App\Criteria;
 */
class DataCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        if ($request = request()->get('search')) {
            $params = explode(';', $request);
            foreach ($params as $p) {
                $col = explode(':', $p);
                if ($col[0] == 'data_viagem_inicial') {
                    $date = Carbon::createFromTimeString($col[1])->format('Y-m-d');
                    $model = $model->whereDate('data_viagem', '>=', $date);
                }
                if ($col[0] == 'data_viagem_final') {
                    $date = Carbon::createFromTimeString($col[1])->format('Y-m-d');
                    $model = $model->whereDate('data_viagem', '<=', $date);
                }
            }
        }
        return $model;
    }
}
