<?php


namespace App\Services\ManagementPoint;

use App\Models\PartnerProduct;
use App\Models\User;
use App\Notifications\TrocaPorProdutoAprovadoNotification;
use App\Notifications\TrocaPorProdutoCanceladoNotification;
use App\Notifications\TrocaPorProdutoPendenteAdminNotification;
use App\Notifications\TrocaPorProdutoPendenteNotification;
use App\Repositories\ManagementPointRepository;
use App\Repositories\PartnerProductRepository;
use App\Services\AbstractService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ManagementPointService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;
    /**
     * @var PartnerProduct
     */
    private $partnerProduct;

    /**
     * ManagementPointService constructor.
     * @param ManagementPointRepository $repository
     * @param PartnerProductRepository $partnerProduct
     */
    public function __construct(
        ManagementPointRepository $repository,
        PartnerProductRepository $partnerProduct
    )
    {
        $this->repository = $repository;
        $this->partnerProduct = $partnerProduct;
    }

    /**
     * Store a newly created Model in storage.
     *
     * @param Request $request
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(Request $request)
    {
        $find = $this->partnerProduct->find($request->get('partner_product_id'));

        $user = request()->user();
        $request->merge([
            'user_id' => $user->id,
            'value' => $request->get('type') == 'P' ? $find->value_points : $find->value,
            'value_points' => $find->value_points,
        ]);

        $admins = User::where('is_admin', 1)->get();

        $data = $this->repository->create($request->all());
        if ($data) {
            Notification::send($user, new TrocaPorProdutoPendenteNotification($find));
//            Notification::send($admins, new TrocaPorProdutoPendenteAdminNotification($find, $user));
        }
        return $data;
    }

    /**
     * Update the specified Model in storage.
     *
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $find = $this->find($id);
        $data = $find->update($request->all());

        if ($data) {
            if ($find->status == 'finalizado') {
                Notification::send($find->user, new TrocaPorProdutoAprovadoNotification($find));
            }
            if ($find->status == 'cancelado') {
                Notification::send($find->user, new TrocaPorProdutoCanceladoNotification($find));
            }
        }
        return $data;
    }
}
