<?php


namespace App\Services\UsersClient;

use App\Models\User;
use App\Notifications\RegisterAdminNotification;
use App\Notifications\RegisterNotification;
use App\Repositories\UserRepository;
use App\Services\AbstractService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class UserService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * Returns a paginated list of Model.
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function all()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->with($this->repository->relationships)->findWhere(['is_admin' => 0]);
        return request()->pagination == 'false' ? $data->all() : $data->paginate();
    }

    /**
     * UserRepository constructor.
     * @param UserRepository $clientRepository
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(UserRepository $clientRepository)
    {
        $this->repository = $clientRepository;
    }

    /**
     * Store a newly created Model in storage.
     *
     * @param Request $request
     *
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(Request $request)
    {
        $user = $this->repository->create($request->all());

        $admins = new User();
//        $admins->email = 'souindicador@maiscanada.com.br';
        $admins->email = 'wellington.alves.gomes@gmail.com';
        $user->notify(new RegisterNotification());
        Notification::send($admins, new RegisterAdminNotification($user));

        return $user;
    }
}
