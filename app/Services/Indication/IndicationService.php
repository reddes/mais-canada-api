<?php


namespace App\Services\Indication;

use App\Criteria\DataCriteria;
use App\Repositories\IndicationRepository;
use App\Services\AbstractService;

class IndicationService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * Returns a paginated list of Model.
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function all()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(app(DataCriteria::class));
        $data = $this->repository->with($this->repository->relationships)->findWhere(['user_id' => request()->user()->id]);
        return request()->pagination == 'false' ? $data->all() : $data->paginate();
    }

    /**
     * IndicationService constructor.
     * @param IndicationRepository $repository
     */
    public function __construct(IndicationRepository $repository)
    {
        $this->repository = $repository;
    }
}
