<?php


namespace App\Services\PartnerProduct;

use App\Repositories\PartnerProductRepository;
use App\Repositories\PartnerProductsFavoriteRepository;
use App\Services\AbstractService;

class PartnerProductService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;
    /**
     * @var PartnerProductsFavoriteRepository
     */
    protected $productsFavorite;

    /**
     * ManagementPointService constructor.
     * @param PartnerProductRepository $repository
     * @param PartnerProductsFavoriteRepository $productsFavorite
     */
    public function __construct(PartnerProductRepository $repository, PartnerProductsFavoriteRepository $productsFavorite)
    {
        $this->repository = $repository;
        $this->productsFavorite = $productsFavorite;
    }

    /**
     * @param $id
     * @return string
     */
    public function addFavorite($id)
    {
        $find = $this->productsFavorite->findWhere(['partner_product_id' => $id])->first();

        if ($find) {
            $find->delete();
            return 'deleted';

        }

        $this->productsFavorite->create([
            'user_id' => request()->user()->id,
            'partner_product_id' => $id,
            'status' => 'S',
        ]);

        return 'created';
    }
}
