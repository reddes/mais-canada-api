<?php


namespace App\Services\Bank;

use App\Repositories\BankRepository;
use App\Services\AbstractService;

class BankService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * UserRepository constructor.
     * @param BankRepository $bankRepository
     */
    public function __construct(BankRepository $bankRepository)
    {
        $this->repository = $bankRepository;
    }
}
