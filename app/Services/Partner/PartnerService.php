<?php


namespace App\Services\Partner;

use App\Models\PartnerProduct;
use App\Repositories\PartnerRepository;
use App\Services\AbstractService;

class PartnerService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;
    /**
     * @var PartnerProduct
     */
    private $partnerProduct;

    /**
     * ManagementPointService constructor.
     * @param PartnerRepository $repository
     * @param PartnerProduct $partnerProduct
     */
    public function __construct(PartnerRepository $repository, PartnerProduct $partnerProduct)
    {
        $this->repository = $repository;
        $this->partnerProduct = $partnerProduct;
    }

    /**
     * @param $request
     * @param $id
     * @return
     * @throws \Exception
     */
    public function saveProducts($request, $id)
    {
        $partner = $this->repository->findWhere(['id' => $id, 'user_id' => $request->user()->id]);

        if (!$partner) {
            throw new \Exception('Erro ao salvar o produto.');
        }

        $params = $request->get('partner_product');
        $params['partner_id'] = $id;
        $params['image'] = $request->get('image');

        $this->partnerProduct->create($params);

        return $this->repository->with($this->repository->relationships)->find($id);
    }

    /**
     * @param $request
     * @param $id
     * @return
     * @throws \Exception
     */
    public function updateProducts($request, $id)
    {
        $partnerProduct = $this->partnerProduct->find($id);
        $params = $request->get('partner_product');
        if ($request->get('image')) {
            $params['image'] = $request->get('image');
        }
        $partnerProduct->update($params);

        return $this->repository->with($this->repository->relationships)->find($partnerProduct->partner_id);
    }

    /**
     * Remove the specified Model from storage.
     *
     * @param int|string $id
     *
     * @return null
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repository->find($id)->delete();

        return $this->repository->with($this->repository->relationships)->all();
    }
}
