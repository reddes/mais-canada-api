<?php


namespace App\Services\Uf;

use App\Repositories\UfRepository;
use App\Services\AbstractService;

class UfService extends AbstractService
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * UfRepository constructor.
     * @param UfRepository $repository
     */
    public function __construct(UfRepository $repository)
    {
        $this->repository = $repository;
    }
}
